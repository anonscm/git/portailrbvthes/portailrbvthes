package fr.sedoo.rbv.thesauri;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.metadata.server.service.thesaurus.ThesauriFactory;
import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public class ThesauriFactoryTest {

	@Test
	public void testThesaurus() throws Exception
	{
		ThesauriFactory factory = new RBVThesauriFactory();
		ArrayList<Thesaurus> thesauri = factory.getThesauri(LocaleUtil.FRENCH);
		Assert.assertNotNull(thesauri);
		Assert.assertEquals("La liste doit contenir 4 thesauri", 4, thesauri.size()) ;
		thesauri = factory.getThesauri(LocaleUtil.ENGLISH);
		Assert.assertNotNull(thesauri);
	}
	
	@Test
	public void testThesaurusItem() throws Exception
	{
		ThesauriFactory factory = new RBVThesauriFactory();
		ArrayList<Thesaurus> thesauri = factory.getThesauri(LocaleUtil.FRENCH);
		String id = thesauri.get(0).getId();
		ArrayList<ThesaurusItem> thesaurusItems = factory.getThesaurusItems(id, LocaleUtil.FRENCH);
		Assert.assertTrue("La liste ne doit pas �tre vide", thesaurusItems.size()>0);
		thesaurusItems = factory.getThesaurusItems(id, LocaleUtil.ENGLISH);
		Assert.assertTrue("La liste ne doit pas �tre vide", thesaurusItems.size()>0);
		id = thesauri.get(1).getId();
		thesaurusItems = factory.getThesaurusItems(id, LocaleUtil.FRENCH);
		Assert.assertTrue("La liste ne doit pas �tre vide", thesaurusItems.size()>0);
		thesaurusItems = factory.getThesaurusItems(id, LocaleUtil.ENGLISH);
		Assert.assertTrue("La liste ne doit pas �tre vide", thesaurusItems.size()>0);
		id = thesauri.get(2).getId();
		thesaurusItems = factory.getThesaurusItems(id, LocaleUtil.FRENCH);
		Assert.assertTrue("La liste ne doit pas �tre vide", thesaurusItems.size()>0);
		thesaurusItems = factory.getThesaurusItems(id, LocaleUtil.ENGLISH);
		Assert.assertTrue("La liste ne doit pas �tre vide", thesaurusItems.size()>0);
		id = thesauri.get(3).getId();
		thesaurusItems = factory.getThesaurusItems(id, LocaleUtil.FRENCH);
		Assert.assertTrue("La liste ne doit pas �tre vide", thesaurusItems.size()>0);
		thesaurusItems = factory.getThesaurusItems(id, LocaleUtil.ENGLISH);
		Assert.assertTrue("La liste ne doit pas �tre vide", thesaurusItems.size()>0);
	}
	
	
}
