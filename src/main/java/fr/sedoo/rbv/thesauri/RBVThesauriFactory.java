package fr.sedoo.rbv.thesauri;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;

import fr.sedoo.metadata.server.service.thesaurus.SingleLevelSkosHandler;
import fr.sedoo.metadata.server.service.thesaurus.ThesauriFactory;
import fr.sedoo.metadata.shared.domain.thesaurus.DefaultSingleLevelThesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.Thesaurus;
import fr.sedoo.metadata.shared.domain.thesaurus.ThesaurusItem;

public class RBVThesauriFactory implements ThesauriFactory{

	private static final String GEOLOGY_THESAURUS="geology";
	private static final String CLIMATE_THESAURUS="climate";
	private static final String LANDUSE_THESAURUS="landuse";
	private static final String MEASUREMENT_TYPE_THESAURUS="measurementtype";


	private static Map<String, ArrayList<Thesaurus>> thesauri = new HashMap<String, ArrayList<Thesaurus>>();

	public ArrayList<Thesaurus> getThesauri(String language)
	{
		ArrayList<Thesaurus> aux = thesauri.get(language);
		if (aux == null)
		{
			aux = initThesauri(language);
			thesauri.put(language, aux);
		}
		return aux;
	}

	private static ArrayList<Thesaurus> initThesauri(String language) 
	{
		InputStream stream = RBVThesauriFactory.class.getResourceAsStream("thesaurus_"+language+".properties");
		Properties properties = new Properties();
		try {
			Reader reader = new InputStreamReader(stream, "UTF-8");
			properties.load(reader);
		} catch (IOException e) {
		}

		DefaultSingleLevelThesaurus geologyThesaurus = new DefaultSingleLevelThesaurus();
		geologyThesaurus.setId(GEOLOGY_THESAURUS);
		geologyThesaurus.setShortLabel(properties.getProperty(GEOLOGY_THESAURUS));

		DefaultSingleLevelThesaurus climateThesaurus = new DefaultSingleLevelThesaurus();
		climateThesaurus.setId(CLIMATE_THESAURUS);
		climateThesaurus.setShortLabel(properties.getProperty(CLIMATE_THESAURUS));
		
		DefaultSingleLevelThesaurus landuseThesaurus = new DefaultSingleLevelThesaurus();
		landuseThesaurus.setId(LANDUSE_THESAURUS);
		landuseThesaurus.setShortLabel(properties.getProperty(LANDUSE_THESAURUS));
		
		DefaultSingleLevelThesaurus measurementTypeThesaurus = new DefaultSingleLevelThesaurus();
		measurementTypeThesaurus.setId(MEASUREMENT_TYPE_THESAURUS);
		measurementTypeThesaurus.setShortLabel(properties.getProperty(MEASUREMENT_TYPE_THESAURUS));

		ArrayList<Thesaurus> aux = new ArrayList<Thesaurus>();
		aux.add(geologyThesaurus);
		aux.add(climateThesaurus);
		aux.add(landuseThesaurus);
		aux.add(measurementTypeThesaurus);

		return aux;
	}

	public  ArrayList<ThesaurusItem> getThesaurusItems(String id, String language)
	{
		ArrayList<ThesaurusItem> result = new ArrayList<ThesaurusItem>();
		String fileName = getFileNameById(id);
		try
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			InputStream stream = RBVThesauriFactory.class.getResourceAsStream(fileName);
			Reader reader = new InputStreamReader(stream, "UTF-8");
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");
			SingleLevelSkosHandler handler = new SingleLevelSkosHandler(language);
			saxParser.parse(is, handler);
			result = handler.getResult();
			Collections.sort(result);
		}
		catch (Exception e)
		{

		}
		return result;
	}

	private static String getFileNameById(String id) 
	{
		if (id.compareTo(GEOLOGY_THESAURUS)==0)
		{
			return GEOLOGY_THESAURUS+".rdf.xml";
		}
		else if (id.compareTo(CLIMATE_THESAURUS)==0)
		{
			return CLIMATE_THESAURUS+".rdf.xml";
		}
		else if (id.compareTo(LANDUSE_THESAURUS)==0)
		{
			return LANDUSE_THESAURUS+".rdf.xml";
		}
		else if (id.compareTo(MEASUREMENT_TYPE_THESAURUS)==0)
		{
			return MEASUREMENT_TYPE_THESAURUS+".rdf.xml";
		}
		else
		{
			return "";
		}
	}


}
